const links = {
	Canvas: [
		{
			Name: "Labirinto",
			Web: "Labirinto"
		},
		{
			Name: "Pong",
			Web: "Pong"
		},
		{
			Name: "Snake",
			Web: "Snake"
		},
		{
			Name: "Space Invaders",
			Web: "SpaceInvaders"
		}
	],
	Phaser: [
		{
			Name: "ApanhaMe",
			Web: "ApanhaMe"
		},
		{
			Name: "Apple Monkey",
			Web: "AppleMonkey"
		},
		{
			Name: "Asteroids",
			Web: "Asteroids"
		},
		{
			Name: "Atira Neles *",
			Web: "AtiraNeles"
		},
		{
			Name: "Breakout *",
			Web: "Breakout"
		},
		{
			Name: "Chrome Dino *",
			Web: "ChromeDino"
		},
		{
			Name: "Coin Master",
			Web: "CoinMaster"
		},
		{
			Name: "Infinity Down *",
			Web: "InfinityDown"
		},
		{
			Name: "Lanes",
			Web: "Lanes"
		},
		{
			Name: "O Passaro *",
			Web: "OPassaro"
		},
		{
			Name: "Pong",
			Web: "Pong"
		},
		{
			Name: "Snake P",
			Web: "Snake"
		},
		{
			Name: "Space Invaders",
			Web: "SpaceInvaders"
		}
	],
	Vanilla: [
		{
			Name: "Adivinha Número",
			Web: "AdivinhaNumero"
		},
		{
			Name: "Slot Machine *",
			Web: "SlotMachine"
		},
		{
			Name: "Space Invaders",
			Web: "SpaceInvaders",
		}
	],
	Others: [
		{
			Name: "Xadrez",
			FrameWork: "Chess & ChessBoard",
			Web: "Chess"
		},
	],
	Labs: [
		{
			Name: "GDevelop",
			FrameWork: "GDevelop",
			externLink: true,
			Web: "https://201flaviosilva.bitbucket.io/src/Labs/GDevelop/index.html",
		},
		{
			Name: "Unity",
			FrameWork: "Unity",
			externLink: true,
			Web: "https://201flaviosilva.bitbucket.io/src/Labs/Unity/index.html",
		},
		{
			Name: "Vanilla",
			FrameWork: "Vanilla JavaScript",
			externLink: true,
			Web: "https://201flaviosilva.bitbucket.io/src/Labs/JS/Jogos/index.html",
		},
		// {
		// 	Name: "Enable 3d",
		// 	FrameWork: "Enable",
		// 	externLink: true,
		// 	Web: "https://enable3d-fixolas.netlify.app/",
		// },
		// {
		// 	Name: "Kaboom",
		// 	FrameWork: "Kaboom",
		// 	externLink: true,
		// 	Web: "https://201flaviosilva.bitbucket.io/src/Labs/Kaboom/index.html",
		// },
		// {
		// 	Name: "Kontra",
		// 	FrameWork: "Kontra",
		// 	externLink: true,
		// 	Web: "https://201flaviosilva.bitbucket.io/src/Labs/Kontra/index.html",
		// },
	]
};

export default links;
